
//
//  CustomRetailerTabBarViewController.swift
//  CatalogApp
//
//  Created by Apple on 16/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CustomRetailerTabBarViewController: UITabBarController {
    
    var navMain = UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = UIColor.clear
        UITabBar.appearance().tintColor = UIColor(hexString: "d7b471")
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Muli", size: 13)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Muli", size: 15
            )!], for: .selected)
        
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let tabViewController1 = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        //        let nav1 = UINavigationController()
        //        nav1.viewControllers = [tabViewController1]
        
        
        let tabViewController2 = mainStoryBoard.instantiateViewController(withIdentifier: "CatalogViewController") as! CatalogViewController
        //        let nav2 = UINavigationController()
        //        nav2.viewControllers = [tabViewController2]
        
        let tabViewController3 = mainStoryBoard.instantiateViewController(withIdentifier: "InquiryProductViewController") as! InquiryProductViewController
        //        let nav3 = UINavigationController()
        //        nav3.viewControllers = [tabViewController3]
        
        
        tabViewController1.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "profile"),tag: 1)
        tabViewController2.tabBarItem = UITabBarItem(title: "Catalog",image:#imageLiteral(resourceName: "catalog") ,tag:2)
        tabViewController3.tabBarItem = UITabBarItem(title: "Contact",image:#imageLiteral(resourceName: "contact") ,tag:3)
        
        
        tabBarController.viewControllers = [tabViewController1,tabViewController2,tabViewController3]
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
