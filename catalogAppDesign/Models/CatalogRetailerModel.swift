//
//  CatalogRetailerModel.swift
//  CatalogApp
//
//  Created by Apple on 16/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CatalogRetailerModel: NSObject {
    
    var strCatalogName = ""
    var strWholesalerName = ""
    var strCatalogId = ""
    var strToUserId = ""
    var strFromUserId = ""
}
