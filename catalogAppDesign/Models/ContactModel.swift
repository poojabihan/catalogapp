//
//  ContactModel.swift
//  CatalogApp
//
//  Created by Apple on 28/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    
    var strId = ""
    var strFName = ""
    var strLName = ""
    var strRole = ""
    var strCompany = ""
    var strMobile = ""
    var strAddress = ""
    var strEmail = ""
    var strEmailVerifiedAt = ""
    var strDeviceId = ""
    var strDeviceToken = ""
    var strDescription = ""
    var strImageUrl = ""
    var strFullName = ""
    var isSelected = false
    
}
