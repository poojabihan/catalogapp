//
//  InquiryListModel.swift
//  CatalogApp
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InquiryListModel: NSObject {
    
    var strCatalogName = ""
    var strCatalogId = ""
    var followUp = false
    var arrEnquiryList = [InquiryModel]()
    
}
