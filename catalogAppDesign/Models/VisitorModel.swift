//
//  VisitorModel.swift
//  CatalogApp
//
//  Created by Apple on 22/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class VisitorModel: NSObject {
    
    var strId = ""
    var strCatalogViewTime = ""
    var strUserName = ""
    var strUserEmail = ""
    var strUserContact = ""
    var strImageUrl = ""
}
