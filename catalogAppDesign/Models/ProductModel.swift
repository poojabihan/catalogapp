//
//  ProductModel.swift
//  CatalogApp
//
//  Created by Apple on 22/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProductModel: NSObject {
    
    var strCatalogId = ""
    var strCategory = ""
    var strDescription = ""
    var strID = ""
    var arrImages = [ProductImageModel]()
    var strPrice = ""
    var strProductName = ""
    var strQuantity = "0"
    
}
