//
//  AuthModle.swift
//  CatalogApp
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AuthModel {
    
    static let sharedInstance = AuthModel()
    
    var name = ""
    var role = ""
    var company = ""
    var email = ""
    var mobile = ""
    var address = ""
    var device_token = ""
    var device_id = ""
    var created_at = ""
    var image = ""
    var updated_at = ""
    var id = ""
    var email_verified_at = ""
    var token = ""
    var description = ""
    
}
