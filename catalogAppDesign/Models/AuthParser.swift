//
//  AuthParser.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthParser: NSObject {
    
    class func parseUserProfile(response : JSON, completionHandler: @escaping (UserProfileModel) -> Void) {
        
        let model = UserProfileModel()
        
        model.strId = response["data"]["id"].stringValue
        model.strName = response["data"]["name"].stringValue
        model.strRole = response["data"]["role"].stringValue
        model.strCompanyName = response["data"]["company"].stringValue
        model.strMobile = response["data"]["mobile"].stringValue
        model.strAddress = response["data"]["address"].stringValue
        model.strEmail = response["data"]["email"].stringValue
        model.strEmailVerifiedAt = response["data"]["email_verified_at"].stringValue
        model.strDeviceId = response["data"]["device_id"].stringValue
        model.strDeviceToken = response["data"]["device_token"].stringValue
        model.strImage = response["data"]["image"].stringValue
        model.strDescription = response["data"]["description"].stringValue
        
        completionHandler(model)
    }
    
    class func parseCalalogList(response : JSON, completionHandler: @escaping ([CatalogModel], Int, Int) -> Void) {
        
        var arr = [CatalogModel]()
        
        for value in response["data"].arrayValue {
            let model = CatalogModel()
            
            model.strCatalogName = value["catalogName"].stringValue
            model.strId = value["id"].stringValue
            model.strUserId = value["userId"].stringValue
            model.strVisitors = value["visitors"].stringValue
            model.strCatalogStatus = value["catalogStatus"].stringValue
            
            arr.append(model)
        }
        
        let totalPageCount = response["totalPageCount"].intValue
        let currentPageCount = response["currentPage"].intValue
        
        completionHandler(arr, totalPageCount, currentPageCount)
    }
    
    class func parseProductList(response : JSON, completionHandler: @escaping ([ProductModel], Int, Int) -> Void) {
        
        var arr = [ProductModel]()
        
        for value in response["data"].arrayValue {
            let model = ProductModel()
            
            model.strCatalogId = value["catalogId"].stringValue
            model.strCategory = value["category"].stringValue
            model.strDescription = value["description"].stringValue
            model.strID = value["id"].stringValue
            model.strPrice = value["price"].stringValue
            model.strProductName = value["productName"].stringValue
            model.strQuantity = value["quantity"].stringValue
            
            for imgValue in value["images"].arrayValue {
                let imageModel = ProductImageModel()
                
                imageModel.strId = imgValue["id"].stringValue
                imageModel.strImageType = imgValue["imageType"].stringValue
                imageModel.strImageUrl = imgValue["imageUrl"].stringValue
                
                model.arrImages.append(imageModel)
            }
            
            arr.append(model)
        }
        
        let totalPageCount = response["totalPageCount"].intValue
        let currentPageCount = response["currentPage"].intValue
        
        completionHandler(arr, totalPageCount, currentPageCount)
    }
    
    class func parseProduct(response : JSON, completionHandler: @escaping (ProductModel) -> Void) {
        
        let model = ProductModel()
        
        model.strCatalogId = response["data"]["catalogId"].stringValue
        model.strCategory = response["data"]["category"].stringValue
        model.strDescription = response["data"]["description"].stringValue
        model.strID = response["data"]["id"].stringValue
        model.strPrice = response["data"]["price"].stringValue
        model.strProductName = response["data"]["productName"].stringValue
        model.strQuantity = response["data"]["quantity"].stringValue
        
        
        completionHandler(model)
    }
    
    class func parseContactList(response : JSON, completionHandler: @escaping ([ContactModel]) -> Void) {
        
        var arr = [ContactModel]()
        
        for value in response["data"].arrayValue {
            let model = ContactModel()
            
            model.strId = value["id"].stringValue
            model.strRole = value["role"].stringValue
            model.strCompany = value["company"].stringValue
            model.strMobile = value["mobile"].stringValue
            model.strAddress = value["address"].stringValue
            model.strEmail = value["email"].stringValue
            model.strEmailVerifiedAt = value["email_verified_at"].stringValue
            model.strDeviceId = value["device_id"].stringValue
            model.strDeviceToken = value["device_token"].stringValue
            model.strDescription = value["description"].stringValue
            model.strImageUrl = value["image"].stringValue
            model.strFullName = value["name"].stringValue
            
            if value["name"].stringValue.components(separatedBy: " ").count > 1 {
                
                model.strFName = value["name"].stringValue.components(separatedBy: " ")[0]
                model.strLName = value["name"].stringValue.components(separatedBy: " ")[1]
                
            }
            else if value["name"].stringValue.components(separatedBy: " ").count > 0 {
                
                model.strFName = value["name"].stringValue.components(separatedBy: " ")[0]
                model.strLName = ""
                
            }
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseInquiryList(response : JSON, completionHandler: @escaping ([InquiryListModel]) -> Void) {
        
        var arr = [InquiryListModel]()
        
        for value in response["data"].arrayValue {
            let modelList = InquiryListModel()
            
            modelList.strCatalogName = value["catalogName"].stringValue
            modelList.strCatalogId = value["catalogId"].stringValue
            modelList.followUp = value["followup"].boolValue

            for inqModel in value["enquiryList"].arrayValue {
                let model = InquiryModel()
                
                model.strId = inqModel["id"].stringValue
                model.strQuantity = inqModel["quantity"].stringValue
                model.strProduct = inqModel["product"].stringValue
                model.strProductDescription = inqModel["productDescription"].stringValue
                model.strRetailerName = inqModel["retailerName"].stringValue
                model.strRetailerId = inqModel["retailerId"].stringValue
                model.strRetailerMobile = inqModel["retailerMobile"].stringValue
                model.followup = inqModel["followup"].boolValue

                modelList.arrEnquiryList.append(model)
            }
            
            arr.append(modelList)
        }
        
        completionHandler(arr)
    }
    
    class func parseInquiryRetailer(response : JSON, completionHandler: @escaping ([InquiryModel]) -> Void) {
        
        var arr = [InquiryModel]()
        
        for inqModel in response["data"].arrayValue {
            let model = InquiryModel()
            
            model.strId = inqModel["id"].stringValue
            model.strQuantity = inqModel["quantity"].stringValue
            model.strCatalogName = inqModel["catalogName"].stringValue
            model.strProduct = inqModel["product"].stringValue
            model.strProductDescription = inqModel["productDescription"].stringValue
            model.strWholesalerName = inqModel["wholesalerName"].stringValue
            model.strWholesalerId = inqModel["wholesalerId"].stringValue
            model.followup = inqModel["followup"].boolValue

            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseCatalogRetailer(response : JSON, completionHandler: @escaping ([CatalogRetailerModel]) -> Void) {
        
        var arr = [CatalogRetailerModel]()
        
        for inqModel in response["data"].arrayValue {
            let model = CatalogRetailerModel()
            
            model.strCatalogName = inqModel["catalogName"].stringValue
            model.strWholesalerName = inqModel["wholesalerName"].stringValue
            model.strCatalogId = inqModel["catalogId"].stringValue
            model.strToUserId = inqModel["toUserId"].stringValue
            model.strFromUserId = inqModel["fromUserId"].stringValue            
            
            arr.append(model)
        }
        
        completionHandler(arr)
    }
    
    class func parseVisitorsList(response : JSON, completionHandler: @escaping ([VisitorModel]) -> Void) {
        
        var arr = [VisitorModel]()
        
        for value in response["data"].arrayValue {
            let modelList = VisitorModel()
            
            modelList.strId = value["id"].stringValue
            modelList.strCatalogViewTime = value["catalogViewTime"].stringValue
            modelList.strUserName = value["userName"].stringValue
            modelList.strUserEmail = value["userEmail"].stringValue
            modelList.strUserContact = value["userContact"].stringValue
            modelList.strImageUrl = value["image"].stringValue
            
            arr.append(modelList)
        }
        
        completionHandler(arr)
    }
    
}
