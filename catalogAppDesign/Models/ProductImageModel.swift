//
//  ProductImageModel.swift
//  CatalogApp
//
//  Created by Apple on 23/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProductImageModel: NSObject {
    
    var strId = ""
    var strImageType = ""
    var strImageUrl = ""
    
}
