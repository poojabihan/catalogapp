//
//  UserProfileModel.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
class UserProfileModel: NSObject {
    
    var strId = ""
    var strName = ""
    var strRole = ""
    var strCompanyName = ""
    var strMobile = ""
    var strAddress = ""
    var strEmail = ""
    var strEmailVerifiedAt = ""
    var strDeviceId = ""
    var strDeviceToken = ""
    var strImage = ""
    var strDescription = ""
}
