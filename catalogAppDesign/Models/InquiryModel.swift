//
//  InquiryModel.swift
//  CatalogApp
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InquiryModel: NSObject {
    
    var strId = ""
    var strQuantity = ""
    var strProduct = ""
    var strProductDescription = ""
    var strRetailerName = ""
    var strRetailerId = ""
    var strRetailerMobile = ""
    var strCatalogName = ""
    var strWholesalerName = ""
    var strWholesalerId = ""
    var followup = false

}
