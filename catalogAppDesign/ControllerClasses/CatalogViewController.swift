//
//  CatalogViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class CatalogViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrCatalogs = [CatalogModel]()
    var selectedCatalogModel = CatalogModel()
    var isEditCalalog = false
    var selectedCatalogToEdit = CatalogModel()
    var totalPageCount = 0
    var currentPageCount = 1
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var txtCatalogTitle: UITextField!
    @IBOutlet weak var createCatalogView: UIView!
    @IBOutlet weak var CreateCatalogCoverView: UIView!
    @IBOutlet weak var tblCatalogs: UITableView!
    @IBOutlet weak var btnSaveCatalog: UIButton!
    @IBOutlet weak var lblCatlogHeading: UILabel!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.createCatalogView.isHidden = true
        self.CreateCatalogCoverView.isHidden = true
        lblCatlogHeading.text = "Create New Catalog"
        // Do any additional setup after loading the view.
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.text = ""
        self.currentPageCount = 1
        self.totalPageCount = 0
        callWebServiceToGetCatalogsList()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchBar.resignFirstResponder()
        self.currentPageCount = 1
        self.totalPageCount = 0
        callWebServiceToGetCatalogsList()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        //        self.currentPageCount = 1
        //        self.totalPageCount = 0
        //        callWebServiceToGetCatalogsList()
        
        if searchText == "" {
            print("UISearchBar.text cleared!")
            self.totalPageCount = 0
            self.currentPageCount = 1
            callWebServiceToGetCatalogsList()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnCreateAction(_ sender: Any) {
        if (txtCatalogTitle.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter catalog name.")
        }
        else {
            if isEditCalalog {
                callWebServiceToUpdateCatalog()
            }
            else {
                callWebServiceToCreateCatalog()
            }
        }
    }
    
    @IBAction func btnSegmentControlAction(_ sender: Any) {
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        
        self.createCatalogView.isHidden = false
        self.CreateCatalogCoverView.isHidden = false
        
        self.isEditCalalog = false
        self.txtCatalogTitle.text = ""
        self.segmentControl.selectedSegmentIndex = 0
        self.btnSaveCatalog.setTitle("CREATE", for: .normal)
        lblCatlogHeading.text = "Create New Catalog"
        
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.createCatalogView.isHidden = true
        self.CreateCatalogCoverView.isHidden = true
        
        self.isEditCalalog = false
        
    }
    
    @objc func btnVisitorsAction(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "VisitorsViewController") as? VisitorsViewController
        rootController!.selectedCatalogId = arrCatalogs[sender.tag].strId
        self.navigationController?.pushViewController(rootController!, animated: false)
    }
    
    @objc func btnMenuAction(sender: UIButton) {
        
        self.view.endEditing(true)
        
        let catModel = arrCatalogs[sender.tag]
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { _ in
            self.createCatalogView.isHidden = false
            self.CreateCatalogCoverView.isHidden = false
            
            self.isEditCalalog = true
            self.selectedCatalogToEdit = catModel
            self.txtCatalogTitle.text = catModel.strCatalogName
            self.segmentControl.selectedSegmentIndex = catModel.strCatalogStatus == "public" ? 0 : 1
            self.btnSaveCatalog.setTitle("UPDATE", for: .normal)
            
            self.lblCatlogHeading.text = "Update Catalog"
            
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            
            let alert  = UIAlertController(title: "Delete", message: "Are you sure you want to delete this catalog?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
                self.callWebServiceToDeleteCatalog(catalogID: catModel.strId)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Share", style: .default, handler: { _ in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootController = storyboard.instantiateViewController(withIdentifier: "RetailersMultiSelectViewController") as? RetailersMultiSelectViewController
            rootController!.strCatalogID = catModel.strId
            self.navigationController?.pushViewController(rootController!, animated: false)
            
        }))
        
        alert.addAction(UIAlertAction(title: "View Products", style: .default, handler: { _ in
            
            self.selectedCatalogModel = self.arrCatalogs[sender.tag]
            self.performSegue(withIdentifier: "toProduct", sender: nil)
        }))
        
//        alert.addAction(UIAlertAction(title: "View Enquiries", style: .default, handler: { _ in
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let rootController = storyboard.instantiateViewController(withIdentifier: "InquiryProductViewController") as? InquiryProductViewController
//            rootController!.strCatalogName = catModel.strCatalogName
//
//            self.navigationController?.pushViewController(rootController!, animated: false)
//        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCatalogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CatalogsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CatalogsTableViewCell") as! CatalogsTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrCatalogs[indexPath.row].strCatalogName.capitalized
        cell.lblVisitors.text = arrCatalogs[indexPath.row].strVisitors
        cell.btnMenu.tag = indexPath.row
        cell.btnMenu.addTarget(self, action: #selector(btnMenuAction(sender:)), for: .touchUpInside)
        
        cell.btnVisitors.tag = indexPath.row
        cell.btnVisitors.addTarget(self, action: #selector(btnVisitorsAction(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCatalogModel = arrCatalogs[indexPath.row]
        self.performSegue(withIdentifier: "toProduct", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = arrCatalogs.count - 1
        if indexPath.row == lastElement {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            if totalPageCount > currentPageCount - 1 {
                callWebServiceToGetCatalogsList()
            }
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToCreateCatalog() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["catalogName": txtCatalogTitle.text ?? "",
                     "userId": AuthModel.sharedInstance.id,
                     "catalogStatus": segmentControl.selectedSegmentIndex == 0 ? "public" : "private"] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.createCatalog, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.isEditCalalog = false
                    self.currentPageCount = 1
                    self.totalPageCount = 0
                    self.arrCatalogs.removeAll()
                    self.createCatalogView.isHidden = true
                    self.CreateCatalogCoverView.isHidden = true
                    self.callWebServiceToGetCatalogsList()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToUpdateCatalog() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["catalogId": selectedCatalogToEdit.strId,
                     "catalogName": txtCatalogTitle.text ?? "",
                     "catalogStatus": segmentControl.selectedSegmentIndex == 0 ? "public" : "private"] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateCatalog, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.isEditCalalog = false
                    
                    self.createCatalogView.isHidden = true
                    self.CreateCatalogCoverView.isHidden = true
                    self.callWebServiceToGetCatalogsList()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToDeleteCatalog(catalogID: String) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["catalogId": catalogID] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.deleteCatalog, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.callWebServiceToGetCatalogsList()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetCatalogsList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": AuthModel.sharedInstance.id,
                     "searchKey": searchBar.text ?? "",
                     "page": String(self.currentPageCount)]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getCatalogsList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseCalalogList(response: response!, completionHandler: { (arr, tpc, cpc) in
                        
                        if self.currentPageCount == 1 {
                            self.arrCatalogs = arr
                        }
                        else{
                            self.arrCatalogs.append(contentsOf: arr)
                        }
                        
                        self.totalPageCount = tpc
                        self.currentPageCount = cpc + 1
                        self.tblCatalogs.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toProduct" {
            let vc = segue.destination as! ProductViewController
            vc.selectedCatalogModel = selectedCatalogModel
        }
    }
}
