//
//  ProductViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class ProductViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: - Variable
    var selectedCatalogModel = CatalogModel()
    let panel = JKNotificationPanel()
    var arrProducts = [ProductModel]()
    var selectedProduct = ProductModel()
    var totalPageCount = 0
    var currentPageCount = 1
    var strWholesalerId = ""
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblProducts: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btnAddProduct: UIButton!
    @IBOutlet var lblNavTitle: UILabel!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tblProducts.estimatedRowHeight = 120.0
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        
        self.lblNavTitle.text = selectedCatalogModel.strCatalogName
        
        if AuthModel.sharedInstance.role == kRetailer {
            btnAddProduct.isHidden = true
            
            let date = Date()
            UserDefaults.standard.set(date, forKey: kStartDate)
            UserDefaults.standard.set(selectedCatalogModel.strId, forKey: kCatalogIDForTimer)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.text = ""
        self.currentPageCount = 1
        self.totalPageCount = 0
        callWebServiceToGetProductsList()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchBar.resignFirstResponder()
        self.currentPageCount = 1
        self.totalPageCount = 0
        callWebServiceToGetProductsList()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        //        self.currentPageCount = 1
        //        self.totalPageCount = 0
        //        callWebServiceToGetCatalogsList()
        
        if searchText == "" {
            print("UISearchBar.text cleared!")
            self.totalPageCount = 0
            self.currentPageCount = 1
            callWebServiceToGetProductsList()
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        
        if AuthModel.sharedInstance.role == kRetailer {
            let date = Date()
            UserDefaults.standard.set(date, forKey: kEndDate)
            
            callWebServiceForViewTime()
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func btnMenuAction(sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { _ in
            self.selectedProduct = self.arrProducts[sender.tag]
            self.performSegue(withIdentifier: "editProduct", sender: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            
            let alert  = UIAlertController(title: "Delete", message: "Are you sure you want to delete this product?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
                self.callWebServiceToDeleteProduct(productID: self.arrProducts[sender.tag].strID)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell") as! ProductTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrProducts[indexPath.row].strProductName.capitalized
        cell.lblSubTitle.text = arrProducts[indexPath.row].strDescription
        
        if arrProducts[indexPath.row].arrImages.count > 0 {
            let url = URL(string:arrProducts[indexPath.row].arrImages[0].strImageUrl)
            cell.imgProduct.kf.setImage(with: url)
        }
        else {
            cell.imgProduct.image = #imageLiteral(resourceName: "noimage")
        }
        
        if AuthModel.sharedInstance.role == kWholesaler {
            cell.btnMenu.tag = indexPath.row
            cell.btnMenu.addTarget(self, action: #selector(btnMenuAction(sender:)), for: .touchUpInside)
        }
        else {
            cell.btnMenu.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if AuthModel.sharedInstance.role == kWholesaler {
            selectedProduct = arrProducts[indexPath.row]
            self.performSegue(withIdentifier: "editProduct", sender: nil)
        }
        else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let rootController = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            rootController?.strWholesalerId = strWholesalerId
            rootController?.productModel = arrProducts[indexPath.row]
            self.navigationController?.pushViewController(rootController!, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = arrProducts.count - 1
        if indexPath.row == lastElement {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            if totalPageCount > currentPageCount - 1 {
                callWebServiceToGetProductsList()
            }
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToGetProductsList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["catalogId": selectedCatalogModel.strId,
                     "searchKey": searchBar.text ?? "",
                     "page": String(self.currentPageCount)]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getProductList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseProductList(response: response!, completionHandler: { (arr, tpc, cpc) in
                        if self.currentPageCount == 1 {
                            self.arrProducts = arr
                        }
                        else{
                            self.arrProducts.append(contentsOf: arr)
                        }
                        
                        self.totalPageCount = tpc
                        self.currentPageCount = cpc + 1
                        
                        self.tblProducts.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToDeleteProduct(productID: String) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["productId": productID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.deleteProduct, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.currentPageCount = 1
                    self.totalPageCount = 0
                    
                    self.callWebServiceToGetProductsList()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForViewTime() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second]
        
        let date1 = UserDefaults.standard.object(forKey: kStartDate) as! Date
        let date2 = UserDefaults.standard.object(forKey: kEndDate) as! Date
        
        let timeInSeconds = formatter.string(from: date1, to: date2)!
        
        let param = ["catalogId": selectedCatalogModel.strId,
                     "timeInSecond": timeInSeconds,
                     "userId": AuthModel.sharedInstance.id]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.addCatalogViewTime, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    UserDefaults.standard.removeObject(forKey:kStartDate)
                    UserDefaults.standard.removeObject(forKey:kEndDate)
                    UserDefaults.standard.removeObject(forKey:kCatalogIDForTimer)
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toAddProduct" {
            let vc = segue.destination as! AddProductViewController
            vc.selectedCatalogModel = selectedCatalogModel
            vc.strNavTitle = "Add Product"
        }
        
        if segue.identifier == "editProduct" {
            let vc = segue.destination as! AddProductViewController
            vc.productModel = selectedProduct
            vc.isEditMode = true
            vc.strNavTitle = "Update Product"
        }
    }
    
    
}
