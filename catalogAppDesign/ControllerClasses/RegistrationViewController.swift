//
//  RegistrationViewController.swift
//  CatalogApp
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON
class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlet
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var segmentRole: UISegmentedControl!
    @IBOutlet weak var btnPasswordEye: UIButton!
    @IBOutlet weak var btnConfirmPasswordEye: UIButton!
    
    //MARK: - Variables
    var selectedRole = ""
    var iconClickPass = true
    var iconClickConfirmPass = true
    var panel = JKNotificationPanel()
    var textField: UITextField?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedRole = "2"
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        btnPasswordEye.isSelected = true
        btnConfirmPasswordEye.isSelected = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowPasswordAction(sender: AnyObject) {
        if(iconClickPass == true) {
            txtPassword.isSecureTextEntry = false
            btnPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnPasswordEye.isSelected = false
            iconClickPass = false
        } else {
            txtPassword.isSecureTextEntry = true
            btnPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnPasswordEye.isSelected = true
            iconClickPass = true
        }
        
    }
    
    @IBAction func btnShowConfirmPasswordAction(sender: AnyObject) {
        
        if(iconClickConfirmPass == true) {
            txtConfirmPassword.isSecureTextEntry = false
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnConfirmPasswordEye.isSelected = false
            iconClickConfirmPass = false
            
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnConfirmPasswordEye.isSelected = true
            iconClickConfirmPass = true
            
        }
        
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        if (txtName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter name.")
        }
        else if (txtCompanyName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter company name.")
        }
        else if (txtContactNumber.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter phone number.")
        }
        else if (txtAddress.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter address.")
        }
        else if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Valid email.")
        }
            
        else if (txtPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Password.")
        }
        else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if txtConfirmPassword.text! != txtPassword.text! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
        }
        else {
            callWebServiceForRegistration()
        }
    }
    
    @IBAction func segmentRoleAction(_ sender: Any) {
        
        switch self.segmentRole.selectedSegmentIndex {
        case 0 :
            print("Wholesaler")
            selectedRole = "2"
        case 1 :
            print("Retailer")
            selectedRole = "3"
        default :
            print("Wholesaler")
            selectedRole = "2"
        }
        
    }
    //MARK: - Call WebService
    
    func callWebServiceForRegistration() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["name":txtName.text ?? "",
                     "role":selectedRole,
                     "company":txtCompanyName.text ?? "",
                     "email":txtEmail.text!.lowercased() ,
                     "mobile":txtContactNumber.text ?? "",
                     "password":txtPassword.text ?? "",
                     "address":txtAddress.text ?? "",
                     "device_token":"",
                     "device_id":UIDevice.current.identifierForVendor!.uuidString,
                     "image":""]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.register, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Registered successfully.")
                    // self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    //MARK: - Custom Methods
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
    }
    
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    //MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
}
