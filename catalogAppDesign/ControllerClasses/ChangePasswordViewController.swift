//
//  ChangePasswordViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class ChangePasswordViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnCurrentPasswordEye: UIButton!
    @IBOutlet weak var btnNewPasswordEye: UIButton!
    @IBOutlet weak var btnConfirmPasswordEye: UIButton!
    
    //MARK: - Variable
    var iconClickCurrentPass = true
    var iconClickNewPass = true
    var iconClickConfirmPass = true
    let panel = JKNotificationPanel()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShowCurrentPasswordAction(sender: AnyObject) {
        if(iconClickCurrentPass == true) {
            print("selected")
            txtCurrentPassword.isSecureTextEntry = false
            btnCurrentPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnCurrentPasswordEye.isSelected = false
            iconClickCurrentPass = false
        } else {
            print("unselected")
            txtCurrentPassword.isSecureTextEntry = true
            btnCurrentPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnCurrentPasswordEye.isSelected = true
            iconClickCurrentPass = true
        }
        
    }
    
    @IBAction func btnShowNewPasswordAction(sender: AnyObject) {
        if(iconClickNewPass == true) {
            print("selected")
            txtNewPassword.isSecureTextEntry = false
            btnNewPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnNewPasswordEye.isSelected = false
            iconClickNewPass = false
        } else {
            print("unselected")
            txtNewPassword.isSecureTextEntry = true
            btnNewPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnNewPasswordEye.isSelected = true
            iconClickNewPass = true
        }
        
    }
    
    @IBAction func btnShowConfirmPasswordAction(sender: AnyObject) {
        if(iconClickConfirmPass == true) {
            print("selected")
            txtConfirmPassword.isSecureTextEntry = false
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnConfirmPasswordEye.isSelected = false
            iconClickConfirmPass = false
        } else {
            print("unselected")
            txtConfirmPassword.isSecureTextEntry = true
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnConfirmPasswordEye.isSelected = true
            iconClickConfirmPass = true
        }
    }
    
    @IBAction func btnSaveAction(sender: AnyObject) {
        if (txtCurrentPassword.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter your current password.")
        }
        else if(checkForSpace(strCheckString: txtCurrentPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if (txtNewPassword.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password should not blank.")
        }
        else if(checkForSpace(strCheckString: txtNewPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if txtConfirmPassword.text! != txtNewPassword.text! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
        }
        else{
            callWebServiceToChangePassword();
        }
    }
    
    //MARK: - Custom Methods
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    //MARK: - WebService Methods
    func callWebServiceToChangePassword() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["currentPassword": txtCurrentPassword.text ?? "",
                     "newPassword": txtNewPassword.text ?? ""] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.changePassword, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
