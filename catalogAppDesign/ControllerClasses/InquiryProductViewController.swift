//
//  InquiryProductViewController.swift
//  CatalogApp
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class InquiryProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblInquiry: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoEnquiry: UILabel!

    //MARK: - Variable
    var arrInquiry = [InquiryModel]()
    var strCatalogName = ""
    let panel = JKNotificationPanel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lblTitle.text = strCatalogName.capitalized
        if arrInquiry.count == 0 {
            lblNoEnquiry.isHidden = false
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Custom Methods
    @objc func btnChatAction(sender: UIButton) {
        openWhatsapp(number: arrInquiry[sender.tag].strRetailerMobile)
    }
    
    @objc func btnCallAction(sender: UIButton) {
        
        let number = arrInquiry[sender.tag].strRetailerMobile
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func openWhatsapp(number: String){
        let urlWhats = "whatsapp://send?phone=" + number
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                } else {
                    print("Install Whatsapp")
                }
            }
        }
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInquiry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : InquiryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InquiryTableViewCell") as! InquiryTableViewCell
        
        cell.selectionStyle = .none
        cell.lblCatalogName.text = strCatalogName.capitalized
        cell.lblProductName.text = arrInquiry[indexPath.row].strProduct.capitalized
        cell.lblQuantity.text = arrInquiry[indexPath.row].strQuantity
        cell.lblDesc.text = arrInquiry[indexPath.row].strProductDescription
        cell.btnChat.tag = indexPath.row
        cell.btnCall.tag = indexPath.row
        cell.btnChat.addTarget(self, action: #selector(btnChatAction(sender:)), for: .touchUpInside)
        cell.btnCall.addTarget(self, action: #selector(btnCallAction(sender:)), for: .touchUpInside)
        
        if arrInquiry[indexPath.row].followup {
            cell.imgFollowUp.isHidden = false
        }
        else {
            cell.imgFollowUp.isHidden = true
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
