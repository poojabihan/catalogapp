//
//  EditProfileViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import SwiftyJSON
import Kingfisher

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtCompanyAddress: UITextField!
    @IBOutlet weak var txtTagline: UITextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - Variable
    var textField: UITextField?
    var imagePicker = UIImagePickerController()
    let panel = JKNotificationPanel()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        imagePicker.delegate = self
        self.hideKeyboardWhenTappedAround()
        self.txtEmail.isUserInteractionEnabled = false
        self.txtEmail.textColor = UIColor.gray
        
        callWebServiceToGetUserDetail()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if (txtName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter name.")
        }
        else if (txtMobileNumber.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter phone number.")
        }
        else if (txtCompanyName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter company name.")
        }
        else if (txtCompanyAddress.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter address.")
        }
        else {
            callWebServiceForUpdateProfile()
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToGetUserDetail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": AuthModel.sharedInstance.id]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        
                        self.txtName.text = model.strName
                        self.txtTagline.text = model.strDescription
                        self.txtEmail.text = model.strEmail.lowercased()
                        self.txtCompanyAddress.text = model.strAddress
                        self.txtCompanyName.text = model.strCompanyName
                        self.txtMobileNumber.text = model.strMobile
                        let url = URL(string:model.strImage)
                        if url == nil {
                            self.imgProfile.image = UIImage(named: "noimage")
                        }
                        else {
                            self.imgProfile.kf.setImage(with: url)
                        }
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func  callWebServiceForUpdateProfile(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["name": txtName.text ?? "",
                     "mobile": txtMobileNumber.text ?? "",
                     "image":  imgProfile.image == nil ? "" : convertImageToBase64(image: imgProfile.image!),
                     "userId": AuthModel.sharedInstance.id,
                     "company" : txtCompanyName.text ?? "",
                     "address": txtCompanyAddress.text ?? "",
                     "description" : txtTagline.text ?? ""] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true  {
                    self.setLoginData(json: response!)
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Details Update Successfully.")
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    /*Action Sheet Options Function for Uploading File*/
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = image.jpeg(.medium) {
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.cameraDevice = .front
            
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            imgProfile.image = image
            self.dismiss(animated: false, completion: nil)
        }
        else{
            
            self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return range.location < 80
    }
    
    func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.address = json["data"]["address"].stringValue
        auth.company = json["data"]["company"].stringValue
        auth.description = json["data"]["description"].stringValue
        auth.device_id = json["data"]["device_id"].stringValue
        auth.device_token = json["data"]["device_token"].stringValue
        auth.email = json["data"]["email"].stringValue.lowercased()
        auth.email_verified_at = json["data"]["email_verified_at"].stringValue
        auth.id = json["data"]["id"].stringValue
        auth.image = json["data"]["image"].stringValue
        auth.mobile = json["data"]["mobile"].stringValue
        auth.name = json["data"]["name"].stringValue
        auth.role = json["data"]["role"].stringValue
        
        let defaults = UserDefaults.standard
        defaults.set(auth.address, forKey: "address")
        defaults.set(auth.company, forKey: "company")
        defaults.set(auth.description, forKey: "description")
        defaults.set(auth.device_id, forKey: "device_id")
        defaults.set(auth.device_token, forKey: "device_token")
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.email_verified_at, forKey: "email_verified_at")
        defaults.set(auth.id, forKey: "id")
        defaults.set(auth.image, forKey: "image")
        defaults.set(auth.mobile, forKey: "mobile")
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.role, forKey: "role")        
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
