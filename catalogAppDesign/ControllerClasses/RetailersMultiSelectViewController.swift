//
//  RetailersMultiSelectViewController.swift
//  CatalogApp
//
//  Created by Apple on 22/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class RetailersMultiSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrContacts = [ContactModel]()
    var arrFilterdContacts = [ContactModel]()
    var selectedCount = 0
    var strCatalogID = ""
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblContacts: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btnSend: UIButton!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        searchBar.showsScopeBar = true
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.text = ""
        callWebServiceToGetProductsList()
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilterdContacts = searchText.isEmpty ? arrContacts : arrContacts.filter { (item: ContactModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strFullName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblContacts.reloadData()
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendAction(sender: AnyObject) {
        callWebServiceToShareCatalog()
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterdContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrFilterdContacts[indexPath.row].strFullName
        cell.lblEmail.text = arrFilterdContacts[indexPath.row].strEmail.lowercased()
        cell.lblPhone.text = arrFilterdContacts[indexPath.row].strMobile
        
        let url = URL(string:arrFilterdContacts[indexPath.row].strImageUrl)
        cell.imgcontact.kf.setImage(with: url)
        
        if arrFilterdContacts[indexPath.row].isSelected {
            cell.imgBg.backgroundColor = UIColor(hexString: "#46D3CACA")
            cell.imgBg.borderWidth = 1.0
            cell.imgBg.borderColor = UIColor(hexString: "#D7B471")
        }
        else {
            cell.imgBg.backgroundColor = UIColor.clear
            cell.imgBg.borderWidth = 0.0
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        arrFilterdContacts[indexPath.row].isSelected = !arrFilterdContacts[indexPath.row].isSelected
        
        if arrFilterdContacts[indexPath.row].isSelected {
            selectedCount = selectedCount + 1
        }
        else {
            selectedCount = selectedCount - 1
        }
        
        if selectedCount > 0 {
            btnSend.isHidden = false
        }
        else {
            btnSend.isHidden = true
        }
        
        tableView.reloadData()
    }
    
    
    //MARK: - WebService Methods
    func callWebServiceToGetProductsList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["wholesalerId": AuthModel.sharedInstance.id]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getRetailerList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseContactList(response: response!, completionHandler: { (arr) in
                        self.arrContacts = arr
                        self.arrFilterdContacts = arr
                        self.tblContacts.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToShareCatalog() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var arrUserId = [String]()
        for value in arrFilterdContacts {
            if value.isSelected {
                arrUserId.append(value.strId)
            }
        }
        
        let param = ["userId": arrUserId,
                     "catalogId": strCatalogID] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.shareCatalog, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    
                    self.btnBackAction(sender: self)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
