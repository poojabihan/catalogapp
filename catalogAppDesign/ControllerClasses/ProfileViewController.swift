//
//  ProfileViewController.swift
//  CatalogApp
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class ProfileViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblPersonName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var heightConstantOfDescription: NSLayoutConstraint!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgProfileDesc: UIImageView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let tapMobile = UITapGestureRecognizer(target: self, action: #selector(lblMobileAction))
        lblMobile.addGestureRecognizer(tapMobile)
        
        let tapEmail = UITapGestureRecognizer(target: self, action: #selector(lblEmailAction))
        lblEmail.addGestureRecognizer(tapEmail)

        let tapDesc = UITapGestureRecognizer(target: self, action: #selector(lblDescAction))
        lblAddress.addGestureRecognizer(tapDesc)
        
        let tapDescFullView = UITapGestureRecognizer(target: self, action: #selector(fullDescViewAction))
        descView.addGestureRecognizer(tapDescFullView)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        callWebServiceToGetUserDetail()
        
    }
    
    //MARK: - Custom Methods
    @objc func lblMobileAction() {
        
        if lblMobile.text != "" {
            if let url = URL(string: "tel://\(lblMobile.text ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func lblEmailAction() {
        if lblEmail.text != "" {
            if let url = URL(string: "mailto:\(lblEmail.text ?? "")") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func lblDescAction() {
        if lblAddress.text != "" {
            lblDesc.text = lblAddress.text
            descView.isHidden = false
            self.imgProfileDesc.image = self.imgProfile.image
        }
    }
    
    @objc func fullDescViewAction() {
            descView.isHidden = true
    }
    
    //MARK: - IBAction
    @IBAction func btnChangePasswordAction(_ sender: Any) {
        
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            self.callWebServiceToLogout()
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController")
        self.navigationController?.pushViewController(rootController, animated: false)
    }
    
    //MARK: - WebService Methods
    func callWebServiceToGetUserDetail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": AuthModel.sharedInstance.id]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.userProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseUserProfile(response: response!, completionHandler: { (model) in
                        
                        self.lblPersonName.text = model.strName
                        
                        if model.strDescription == ""{
                            self.heightConstantOfDescription.constant = 0
                        }else{
                            
                            self.heightConstantOfDescription.constant = 40
                            self.lblAddress.text = model.strDescription
                            
                        }
                        self.lblEmail.text = model.strEmail.lowercased()
                        self.lblLocation.text = model.strAddress
                        self.lblCompanyName.text = model.strCompanyName
                        self.lblMobile.text = model.strMobile
                        
                        let url = URL(string:model.strImage)
                        if url == nil {
                            self.imgProfile.image = UIImage(named: "noimage")
                        }
                        else {
                            self.imgProfile.kf.setImage(with: url)
                        }
                        //                        self.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "noprofile")  , progressBlock:nil, completionHandler: nil)
                        
                    })
                    
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToLogout() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: ["":""], withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    let defaults = UserDefaults.standard
                    defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootController = storyboard.instantiateViewController(withIdentifier: "rootLogin")
                    self.appDelegate.window?.rootViewController = rootController
                    self.appDelegate.window?.makeKeyAndVisible()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    
}
