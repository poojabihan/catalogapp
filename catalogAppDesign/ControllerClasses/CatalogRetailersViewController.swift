//
//  CatalogRetailersViewController.swift
//  CatalogApp
//
//  Created by Apple on 16/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class CatalogRetailersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblCatalogs: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrCatalogs = [CatalogRetailerModel]()
    var arrFilteredCatalogs = [CatalogRetailerModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.text = ""
        callWebServiceToListCatalogs()
        
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilteredCatalogs = searchText.isEmpty ? arrCatalogs : arrCatalogs.filter { (item: CatalogRetailerModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strCatalogName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblCatalogs.reloadData()
    }
    
    
    //MARK: - WebService Methods
    func callWebServiceToListCatalogs() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": AuthModel.sharedInstance.id] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getSharedCatalogList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseCatalogRetailer(response: response!, completionHandler: { (arr) in
                        self.arrCatalogs = arr
                        self.arrFilteredCatalogs = arr
                        self.tblCatalogs.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilteredCatalogs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CatalogsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CatalogsTableViewCell") as! CatalogsTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrFilteredCatalogs[indexPath.row].strCatalogName.capitalized
        cell.lblWholesaler.text = "Wholesaler: " + arrFilteredCatalogs[indexPath.row].strWholesalerName
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "ProductViewController") as? ProductViewController
        
        let model = CatalogModel()
        model.strId = arrFilteredCatalogs[indexPath.row].strCatalogId
        model.strCatalogName = arrFilteredCatalogs[indexPath.row].strCatalogName
        model.strUserId = arrFilteredCatalogs[indexPath.row].strToUserId
        rootController?.selectedCatalogModel = model
        rootController?.strWholesalerId = arrFilteredCatalogs[indexPath.row].strFromUserId
        self.navigationController?.pushViewController(rootController!, animated: false)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
