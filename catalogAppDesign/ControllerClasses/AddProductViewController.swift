//
//  AddProductViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class AddProductViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK: - Variable
    var selectedCatalogModel = CatalogModel()
    let panel = JKNotificationPanel()
    var imagePicker = UIImagePickerController()
    var imgArray = [UIImage]()
    var productModel = ProductModel()
    var selectedImgArray = [ProductImageModel]()
    var isEditMode = false
    var arrImgsToDelete = [String]()
    var strNavTitle = ""
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtProductName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtDecription: UITextView!
    @IBOutlet weak var addImageBigView: UIView!
    @IBOutlet weak var addImageSmallView: UIView!
    @IBOutlet weak var imgCollectionView: UICollectionView!
    @IBOutlet weak var lblNavTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        imagePicker.delegate = self
        
        lblNavTitle.text = strNavTitle
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    func setData() {
        if isEditMode {
            txtProductName.text = productModel.strProductName
            txtPrice.text = productModel.strPrice
            txtCategory.text = productModel.strCategory
            txtQuantity.text = productModel.strQuantity
            txtDecription.text = productModel.strDescription
            selectedImgArray = productModel.arrImages
            imgCollectionView.reloadData()
            addImageBigView.isHidden = true
            addImageSmallView.isHidden = false
        }
        else {
            addImageBigView.isHidden = false
            addImageSmallView.isHidden = true
        }
    }
    /*Action Sheet Options Function for Uploading File*/
    
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = image.jpeg(.medium) {
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func btnDeleteAction(sender: UIButton) {
        
        if isEditMode {
            
            if imgArray.count > 0 && selectedImgArray.count <= sender.tag{
                imgArray.remove(at: sender.tag - selectedImgArray.count)
                imgCollectionView.reloadData()
            }
            else {
                arrImgsToDelete.append(selectedImgArray[sender.tag].strId)
                selectedImgArray.remove(at: sender.tag)
                imgCollectionView.reloadData()
            }
        }
        else {
            imgArray.remove(at: sender.tag)
            imgCollectionView.reloadData()
        }
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction(sender: AnyObject) {
        if (txtProductName.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter product name.")
        }
        else if (txtPrice.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter price.")
        }
        else if (txtCategory.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter category.")
        }
        else if (txtQuantity.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter quantity.")
        }
        else if (txtDecription.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter description.")
        }
        else {
            if isEditMode {
                callWebServiceToUpdateProduct(imgArrToDelete: arrImgsToDelete)
            }
            else {
                callWebServiceToCreateProduct()
            }
        }
    }
    
    @IBAction func btnAddImageAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imgArray.append(image)
            addImageBigView.isHidden = true
            addImageSmallView.isHidden = false
            
            imgCollectionView.reloadData()
            
            self.dismiss(animated: false, completion: nil)
        }
        else{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - UICollectonView Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isEditMode {
            if imgArray.count > 0 {
                return selectedImgArray.count + imgArray.count
            }
            return selectedImgArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImageCollectionViewCell", for: indexPath) as! ProductImageCollectionViewCell
        
        if isEditMode {
            
            if imgArray.count > 0 && selectedImgArray.count <= indexPath.row{
                cell.imgProduct.image = imgArray[indexPath.row - selectedImgArray.count]
            }
            else {
                let url = URL(string:selectedImgArray[indexPath.row].strImageUrl)
                cell.imgProduct.kf.setImage(with: url)
            }
        }
        else {
            cell.imgProduct.image = imgArray[indexPath.row]
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    //MARK: - WebService Methods
    func callWebServiceToCreateProduct() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["productName": txtProductName.text ?? "",
                     "catalogId": selectedCatalogModel.strId,
                     "price": txtPrice.text ?? "",
                     "category": txtCategory.text ?? "",
                     "quantity": txtQuantity.text ?? "",
                     "description": txtDecription.text ?? ""] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.addProduct, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseProduct(response: response!, completionHandler: { (model) in
                        self.productModel = model
                    })
                    
                    if self.imgArray.count > 0 {
                        self.callWebServiceToUploadProductImage(selectedImage: self.imgArray[0], index: 0)
                    }
                    else {
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToUpdateProduct(imgArrToDelete: [String]) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["productId": productModel.strID,
                     "imageDeleteID": imgArrToDelete,
                     "productName": txtProductName.text ?? "",
                     "price": txtPrice.text ?? "",
                     "category": txtCategory.text ?? "",
                     "quantity": txtQuantity.text ?? "",
                     "description": txtDecription.text ?? ""] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.updateProduct, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    if self.imgArray.count > 0 {
                        self.callWebServiceToUploadProductImage(selectedImage: self.imgArray[0], index: 0)
                    }
                    else {
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToUploadProductImage(selectedImage: UIImage, index: Int) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["imageTypeId": productModel.strID,
                     "imageType": "product",
                     "image": convertImageToBase64(image: selectedImage)] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.uploadProductImage, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    if index == self.imgArray.count - 1 {
                        self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self.callWebServiceToUploadProductImage(selectedImage: self.imgArray[index + 1], index: index + 1)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
