//
//  VisitorsViewController.swift
//  CatalogApp
//
//  Created by Apple on 22/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class VisitorsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrVisitors = [VisitorModel]()
    var arrFilterdVisitors = [VisitorModel]()
    var selectedCatalogId = ""
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblVisitors: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.text = ""
        callWebServiceToGetVisitorList()
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilterdVisitors = searchText.isEmpty ? arrVisitors : arrVisitors.filter { (item: VisitorModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strUserName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblVisitors.reloadData()
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterdVisitors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrFilterdVisitors[indexPath.row].strUserName
        cell.lblEmail.text = arrFilterdVisitors[indexPath.row].strUserEmail.lowercased()
        cell.lblPhone.text = arrFilterdVisitors[indexPath.row].strUserContact
        cell.lblTimeStayed.text = "Time stayed - " + arrFilterdVisitors[indexPath.row].strCatalogViewTime
        
        let url = URL(string:arrFilterdVisitors[indexPath.row].strImageUrl)
        cell.imgcontact.kf.setImage(with: url)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    //MARK: - WebService Methods
    func callWebServiceToGetVisitorList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["catalogId": selectedCatalogId]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getVisitorList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseVisitorsList(response: response!, completionHandler: { (arr) in
                        self.arrVisitors = arr
                        self.arrFilterdVisitors = arr
                        self.tblVisitors.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
