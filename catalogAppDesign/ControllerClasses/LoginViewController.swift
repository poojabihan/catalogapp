//
//  LoginViewController.swift
//  CatalogApp
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnPasswordEye: UIButton!
    @IBOutlet weak var roleSegment: UISegmentedControl!
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - Variable
    var iconClickPass = true
    var selectedRole = kWholesaler
    let panel = JKNotificationPanel()
    var textField: UITextField?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        btnPasswordEye.isSelected = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    //MARK: - IBAction
    @IBAction func btnShowPasswordAction(sender: AnyObject) {
        if(iconClickPass == true) {
            print("selected")
            txtPassword.isSecureTextEntry = false
            btnPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnPasswordEye.isSelected = false
            iconClickPass = false
        } else {
            print("unselected")
            txtPassword.isSecureTextEntry = true
            btnPasswordEye.setImage(#imageLiteral(resourceName: "unhide"), for: .normal)
            btnPasswordEye.isSelected = true
            iconClickPass = true
        }
        
    }
    
    @IBAction func roleSegmentAction(_ sender: Any) {
        
        /*CatalogApp Roles: 1=Superadmin 2=Wholesaler
         3=Retailer
         */
        if roleSegment.selectedSegmentIndex == 0 {
            selectedRole = kWholesaler
        }
        if roleSegment.selectedSegmentIndex == 1{
            selectedRole = kRetailer
        }
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter username.")
        }
        else if (txtEmail.text?.isValidEmail())! {
            if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter password.")
            }
            else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
                
                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
            }
            else {
                callWebServiceForLogin()
            }
        }
        else {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid username.")
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLogin() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["email":txtEmail.text!.lowercased() ,
                     "password":txtPassword.text ?? "",
                     "role": selectedRole]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    print(response!)
                    self.setLoginData(json: response!)
                    //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    //                    let rootController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarViewController")
                    //                    self.navigationController?.pushViewController(rootController, animated: false)
                    
                    if AuthModel.sharedInstance.role == kWholesaler //Wholeseller
                    {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarViewController") as? UITabBarController
                        rootController!.selectedIndex = 1
                        self.appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                        self.appDelegate.window?.rootViewController = rootController
                        self.appDelegate.window?.makeKeyAndVisible()
                        
                    }
                    else {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootController = storyboard.instantiateViewController(withIdentifier: "CustomRetailerTabBarViewController") as? UITabBarController
                        rootController!.selectedIndex = 1
                        self.appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                        self.appDelegate.window?.rootViewController = rootController
                        self.appDelegate.window?.makeKeyAndVisible()
                        
                    }
                    // self.performSegue(withIdentifier: "NavigateToProfile", sender: nil)
                    
                    //NavigateToRetailer
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - Custom Methods
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
    }
    
    func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.name = json["data"]["name"].stringValue
        auth.role = json["data"]["role"].stringValue
        auth.company = json["data"]["company"].stringValue
        auth.email = json["data"]["email"].stringValue
        auth.mobile = json["data"]["mobile"].stringValue
        auth.address = json["data"]["address"].stringValue
        auth.device_token = json["data"]["device_token"].stringValue
        auth.device_id = json["data"]["device_id"].stringValue
        auth.created_at = json["data"]["created_at"].stringValue
        auth.image = json["data"]["image"].stringValue
        auth.updated_at = json["data"]["updated_at"].stringValue
        auth.id = json["data"]["id"].stringValue
        auth.email_verified_at = json["data"]["email_verified_at"].stringValue
        auth.token = json["data"]["token"].stringValue
        auth.description = json["data"]["description"].stringValue
        
        let defaults = UserDefaults.standard
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.role, forKey: "role")
        defaults.set(auth.company, forKey: "company")
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.address, forKey: "address")
        defaults.set(auth.device_token, forKey: "device_token")
        defaults.set(auth.device_id, forKey: "device_id")
        defaults.set(auth.created_at, forKey: "created_at")
        defaults.set(auth.image, forKey: "image")
        defaults.set(auth.updated_at, forKey: "updated_at")
        defaults.set(auth.id, forKey: "id")
        defaults.set(auth.email_verified_at, forKey: "email_verified_at")
        defaults.set(auth.token, forKey: "token")
        defaults.set(auth.description, forKey: "description")
        
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    //MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
