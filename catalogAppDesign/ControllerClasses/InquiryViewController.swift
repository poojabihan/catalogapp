//
//  InquiryViewController.swift
//  CatalogApp
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class InquiryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblInquiry: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrInquiry = [InquiryListModel]()
    var arrFilterdInquiry = [InquiryListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.text = ""
        callWebServiceToListInquiryWholeseller()
        
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilterdInquiry = searchText.isEmpty ? arrInquiry : arrInquiry.filter { (item: InquiryListModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strCatalogName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblInquiry.reloadData()
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterdInquiry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CatalogsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CatalogsTableViewCell") as! CatalogsTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrFilterdInquiry[indexPath.row].strCatalogName
        
        if arrFilterdInquiry[indexPath.row].followUp {
            cell.lblFollowUpStatus.isHidden = false
        }
        else {
            cell.lblFollowUpStatus.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "InquiryProductViewController") as? InquiryProductViewController
        rootController?.strCatalogName = arrFilterdInquiry[indexPath.row].strCatalogName
        rootController?.arrInquiry = arrFilterdInquiry[indexPath.row].arrEnquiryList
        self.navigationController?.pushViewController(rootController!, animated: false)
        
    }
    
    //MARK: - WebService Methods
    func callWebServiceToListInquiryWholeseller() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["wholesalerId": AuthModel.sharedInstance.id] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getListEnquiries, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseInquiryList(response: response!, completionHandler: { (arr) in
                        self.arrInquiry = arr
                        self.arrFilterdInquiry = arr
                        
                        self.tblInquiry.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
