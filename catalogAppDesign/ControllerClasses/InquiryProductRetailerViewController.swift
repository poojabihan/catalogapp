//
//  InquiryProductRetailerViewController.swift
//  CatalogApp
//
//  Created by Apple on 16/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class InquiryProductRetailerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblInquiry: UITableView!
    @IBOutlet var searchBar: UISearchBar!

    //MARK: - Variable
    var arrInquiry = [InquiryModel]()
    var arrFilteredInquiry = [InquiryModel]()
    let panel = JKNotificationPanel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        searchBar.showsScopeBar = true
        searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.text = ""

        callWebServiceToListInquiry()
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilteredInquiry = searchText.isEmpty ? arrInquiry : arrInquiry.filter { (item: InquiryModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strCatalogName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblInquiry.reloadData()
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilteredInquiry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : InquiryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InquiryTableViewCell") as! InquiryTableViewCell
        
        cell.selectionStyle = .none
        cell.lblCatalogName.text = arrFilteredInquiry[indexPath.row].strCatalogName.capitalized
        cell.lblProductName.text = arrFilteredInquiry[indexPath.row].strProduct.capitalized
        cell.lblQuantity.text = arrFilteredInquiry[indexPath.row].strQuantity
        cell.lblDesc.text = arrFilteredInquiry[indexPath.row].strProductDescription
        
        if arrFilteredInquiry[indexPath.row].followup {
            cell.btnFollowUp.setTitle("FOLLOWED UP", for: .normal)
        }
        else {
            cell.btnFollowUp.setTitle("FOLLOW UP", for: .normal)
        }
        
        cell.btnFollowUp.tag = indexPath.row
        cell.btnFollowUp.addTarget(self, action: #selector(btnFollowUpAction(_:)), for: .touchUpInside)

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    //MARK: - Custom Methods
    @objc func btnFollowUpAction(_ sender: UIButton) {
        print("btnFollowUpAction Called")
        
        if !arrFilteredInquiry[sender.tag].followup {
            callWebServiceToFollowUpInquiry(selectedIndex: sender.tag)
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToListInquiry() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": AuthModel.sharedInstance.id] as [String : Any]
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getRetailersEnquiryList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseInquiryRetailer(response: response!, completionHandler: { (arr) in
                        self.arrInquiry = arr
                        self.arrFilteredInquiry = arr
                        self.tblInquiry.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToFollowUpInquiry(selectedIndex: Int) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["enquiryId": arrFilteredInquiry[selectedIndex].strId] as [String : Any]
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.doFollowupEnquiry, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)

                    self.arrFilteredInquiry[selectedIndex].followup = true
                    self.tblInquiry.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
