//
//  ContactViewController.swift
//  CatalogApp
//
//  Created by Apple on 09/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher

class ContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrContacts = [ContactModel]()
    var selectedContact = ContactModel()
    var arrFilterdContacts = [ContactModel]()
    
    //MARK: - IBOutlets
    @IBOutlet weak var tblContacts: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.text = ""
        callWebServiceToGetProductsList()
    }
    
    //MARK: - UISearchBar Methods
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        arrFilterdContacts = searchText.isEmpty ? arrContacts : arrContacts.filter { (item: ContactModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strFullName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblContacts.reloadData()
    }
    
    //MARK: - IBAction
    @IBAction func btnBackAction(sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnMenuAction(sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { _ in
            self.selectedContact = self.arrFilterdContacts[sender.tag]
            self.performSegue(withIdentifier: "editContact", sender: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
            
            let alert  = UIAlertController(title: "Delete", message: "Are you sure you want to delete this retailer?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
                self.callWebServiceToDeleteContact(contactID: self.arrFilterdContacts[sender.tag].strId)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (alert) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilterdContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ContactTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrFilterdContacts[indexPath.row].strFullName
        cell.lblEmail.text = arrFilterdContacts[indexPath.row].strEmail.lowercased()
        cell.lblPhone.text = arrFilterdContacts[indexPath.row].strMobile
        
        cell.btnMenu.tag = indexPath.row
        cell.btnMenu.addTarget(self, action: #selector(btnMenuAction(sender:)), for: .touchUpInside)
        
        let url = URL(string:arrFilterdContacts[indexPath.row].strImageUrl)
        cell.imgcontact.kf.setImage(with: url)        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedContact = arrFilterdContacts[indexPath.row]
        self.performSegue(withIdentifier: "editContact", sender: nil)
    }
    
    
    //MARK: - WebService Methods
    func callWebServiceToGetProductsList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["wholesalerId": AuthModel.sharedInstance.id]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.getRetailerList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    AuthParser.parseContactList(response: response!, completionHandler: { (arr) in
                        self.arrContacts = arr
                        self.arrFilterdContacts = arr
                        self.tblContacts.reloadData()
                    })
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToDeleteContact(contactID: String) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["userId": contactID]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.deleteRetailer, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.callWebServiceToGetProductsList()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "editContact" {
            let vc = segue.destination as! AddContactViewController
            vc.isEditMode = true
            vc.contactModel = selectedContact
            vc.strNavTitle = "Update Retailer"
        }
    }
    
    
}
