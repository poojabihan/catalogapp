//
//  CatalogsTableViewCell.swift
//  CatalogApp
//
//  Created by Apple on 22/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class CatalogsTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var lblVisitors: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWholesaler: UILabel!
    @IBOutlet weak var btnVisitors: UIButton!
    @IBOutlet weak var lblFollowUpStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
