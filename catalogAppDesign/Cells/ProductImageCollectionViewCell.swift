//
//  ProductImageCollectionViewCell.swift
//  CatalogApp
//
//  Created by Apple on 23/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ProductImageCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    
}
