//
//  ContactTableViewCell.swift
//  CatalogApp
//
//  Created by Apple on 28/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgcontact: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblTimeStayed: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
