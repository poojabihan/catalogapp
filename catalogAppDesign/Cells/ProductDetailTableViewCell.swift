//
//  ProductDetailTableViewCell.swift
//  CatalogApp
//
//  Created by Apple on 17/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Kingfisher

class ProductDetailTableViewCell: UITableViewCell, UIScrollViewDelegate {
    
    //MARK: - Variables
    var productModel = ProductModel()
    var currentPage = 0
    var objProductDetail = ProductDetailViewController()
    var quantity = 0
    var zoomImageView = UIImageView()
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgViewSlider: UIImageView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnSendQuery: UIButton!
    @IBOutlet weak var lblNoImg: UILabel!
    
    //MARK: - Life Cycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(btnImageAction(_:)))
        imgViewSlider.addGestureRecognizer(tapGesture)
        
        let leftRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeLeftAction(sender:)))
        leftRecognizer.direction = .left
        
        let rightRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeRightAction(sender:)))
        rightRecognizer.direction = .right
        
        switch productModel.arrImages.count {
        case 0:
            self.pageController.isHidden = true
            self.imgViewSlider.isHidden = true
            self.lblNoImg.isHidden = false
            break
        case 1:
            self.pageController.isHidden = true
            let url = URL(string: productModel.arrImages[currentPage].strImageUrl)
            self.imgViewSlider.kf.setImage(with: url)
            self.lblNoImg.isHidden = true
        default:
            self.pageController.numberOfPages = productModel.arrImages.count
            let url = URL(string: productModel.arrImages[currentPage].strImageUrl)
            self.imgViewSlider.kf.setImage(with: url)
            self.lblNoImg.isHidden = true
            self.imgViewSlider.addGestureRecognizer(leftRecognizer)
            self.imgViewSlider.addGestureRecognizer(rightRecognizer)
        }
        
        
        //        if productModel.arrImages.count > 0 {
        //            self.pageController.numberOfPages = productModel.arrImages.count
        //
        //            let url = URL(string: productModel.arrImages[currentPage].strImageUrl)
        //            self.imgViewSlider.kf.setImage(with: url)
        //
        //            self.lblNoImg.isHidden = true
        //        }
        //        else {
        //            self.pageController.isHidden = true
        //            self.imgViewSlider.isHidden = true
        //            self.lblNoImg.isHidden = false
        //        }
        
        self.lblProductName.text = productModel.strProductName.capitalized
        self.lblAvailability.text = productModel.strQuantity
        self.lblPrice.text = "₹" + productModel.strPrice
        self.lblDesc.text = productModel.strDescription
        
        if Int(productModel.strQuantity)! > 0 {
            self.lblQuantity.text = "1"
            quantity = 1
        }
        else {
            self.lblQuantity.text = "0"
            quantity = 0
        }
        
        if productModel.strQuantity == "0" {
            self.lblQuantity.text = "0"
            quantity = 0
            self.btnSendQuery.isUserInteractionEnabled = false
            self.btnSendQuery.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnSendQuery.borderColorForBtn = UIColor.darkGray
        }
        if productModel.strQuantity == "0" || productModel.strQuantity == "1"{
            self.btnMinus.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnMinus.isUserInteractionEnabled = false
            
            self.btnPlus.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnPlus.isUserInteractionEnabled = false
        }
        else {
            self.btnMinus.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnMinus.isUserInteractionEnabled = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - IBActions
    @IBAction func btnMinusAction( sender: Any) {
        
        quantity = quantity - 1
        
        if quantity == 0 || quantity == 1 {
            self.btnMinus.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnMinus.isUserInteractionEnabled = false
        }
        else {
            self.btnMinus.setTitleColor(UIColor(hexString: "#D7B471"), for: .normal)
            self.btnMinus.isUserInteractionEnabled = true
            
            self.btnPlus.isUserInteractionEnabled = true
            self.btnPlus.setTitleColor(UIColor(hexString: "#D7B471"), for: .normal)
            
        }
        lblQuantity.text = String(quantity)
    }
    
    @IBAction func btnPlusAction( sender: Any) {
        
        quantity = quantity + 1
        
        if quantity == Int(productModel.strQuantity) {
            self.btnPlus.setTitleColor(UIColor.darkGray, for: .normal)
            self.btnPlus.isUserInteractionEnabled = false
        }
        else {
            self.btnPlus.isUserInteractionEnabled = true
            self.btnPlus.setTitleColor(UIColor(hexString: "#D7B471"), for: .normal)
            
            self.btnMinus.isUserInteractionEnabled = true
            self.btnMinus.setTitleColor(UIColor(hexString: "#D7B471"), for: .normal)
            
        }
        
        lblQuantity.text = String(quantity)
    }
    
    @objc func swipeRightAction( sender: Any) {
        
        if currentPage != 0 {
            currentPage = currentPage - 1
            
            let url = URL(string: productModel.arrImages[currentPage].strImageUrl)
            self.imgViewSlider.kf.setImage(with: url)
            
            pageController.currentPage = currentPage
        }
    }
    
    @objc func swipeLeftAction( sender: Any) {
        
        if currentPage < productModel.arrImages.count - 1 {
            currentPage = currentPage + 1
            
            let url = URL(string: productModel.arrImages[currentPage].strImageUrl)
            self.imgViewSlider.kf.setImage(with: url)
            
            pageController.currentPage = currentPage
        }
    }
    
    @IBAction func btnSendQueryAction() {
        objProductDetail.callWebServiceToSendQuery(quantity: lblQuantity.text!)
    }
    
    @objc func btnImageAction(_ sender: UITapGestureRecognizer) {
        
        let imageView = sender.view as! UIImageView
        zoomImageView = UIImageView(image: imageView.image)
        zoomImageView.frame = UIScreen.main.bounds
        zoomImageView.backgroundColor = .black
        zoomImageView.contentMode = .scaleAspectFit
        zoomImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        
        let scrollView = UIScrollView()
        scrollView.frame = UIScreen.main.bounds
        scrollView.delegate = self
        scrollView.addGestureRecognizer(tap)
        
        scrollView.addSubview(zoomImageView)
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        objProductDetail.view.addSubview(scrollView)
        objProductDetail.tabBarController?.tabBar.isHidden = true
        
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        objProductDetail.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - UIScrollView Delegates
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return zoomImageView
    }
}
