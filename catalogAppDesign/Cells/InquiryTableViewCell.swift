//
//  InquiryTableViewCell.swift
//  CatalogApp
//
//  Created by Apple on 15/07/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class InquiryTableViewCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblCatalogName: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnChatHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnFollowUp: UIButton!
    @IBOutlet weak var imgFollowUp: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
