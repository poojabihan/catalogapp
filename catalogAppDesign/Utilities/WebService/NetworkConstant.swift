//
//  NetworkConstant.swift
//  CatalogApp
//
//  Created by Apple on 08/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

let kBaseURL = "http://production.chetaru.co.uk/catalog/api/"

let kWholesaler = "2"
let kRetailer = "3"
let kStartDate = "startDate"
let kEndDate = "endDate"
let kCatalogIDForTimer = "catalogId"

class NetworkConstant: NSObject {
    
    struct Auth {
        static let login = "login"
        static let register = "register"
        static let userProfile = "getUserProfile"
        static let updateProfile = "updateUserProfile"
        static let forgotPassword = "forgotPassword"
        static let getCatalogsList = "getCatalogsList"
        static let createCatalog = "createCatalog"
        static let getProductList = "getProductList"
        static let addProduct = "addProduct"
        static let uploadProductImage = "uploadProductImage"
        static let updateProduct = "updateProduct"
        static let deleteCatalog = "deleteCatalog"
        static let updateCatalog = "updateCatalog"
        static let deleteProduct = "deleteProduct"
        static let changePassword = "updatePasswordWithCurrentPassword"
        static let logout = "logout"
        static let addRetailer = "addRetailer"
        static let getRetailerList = "getRetailerList"
        static let deleteRetailer = "deleteRetailer"
        static let updateRetailer = "updateRetailer"
        static let getListEnquiries = "getListEnquiries"
        static let getRetailersEnquiryList = "getRetailersEnquiryList"
        static let getSharedCatalogList = "getSharedCatalogList"
        static let addProductEnquiry = "addProductEnquiry"
        static let shareCatalog = "shareCatalog"
        static let addCatalogViewTime = "addCatalogViewTime"
        static let getVisitorList = "getVisitorList"
        static let doFollowupEnquiry = "doEnquiryFollowup"

    }
}
