//
//  AppDelegate.swift
//  catalogAppDesign
//
//  Created by Apple on 03/05/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        
        // Override point for customization after application launch.
        let defaults = UserDefaults.standard

        if AuthModel.sharedInstance.id.isEmpty {
            if defaults.value(forKey: "id") != nil {
            //direct login
            setLoginData()
           
                if AuthModel.sharedInstance.role == kWholesaler //Wholeseller
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootController = storyboard.instantiateViewController(withIdentifier: "CustomTabBarViewController") as? UITabBarController
                    rootController!.selectedIndex = 1
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = rootController
                    self.window?.makeKeyAndVisible()

                }
                else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootController = storyboard.instantiateViewController(withIdentifier: "CustomRetailerTabBarViewController") as? UITabBarController
                    rootController!.selectedIndex = 1
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = rootController
                    self.window?.makeKeyAndVisible()
                    
                }
                
             /*   let tabBarController = UITabBarController()
                tabBarController.tabBar.barTintColor = UIColor.clear
                UITabBar.appearance().tintColor = UIColor(hexString: "d7b471")
                
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Muli", size: 13)!], for: .normal)
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Muli", size: 15
                    )!], for: .selected)
            
                
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                
                let tabViewController1 = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                
                let tabViewController2 = mainStoryBoard.instantiateViewController(withIdentifier: "CatalogViewController") as! CatalogViewController
                
                let tabViewController3 = mainStoryBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
                
            
                tabViewController1.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "profile"),tag: 1)
                tabViewController2.tabBarItem = UITabBarItem(title: "Catalog",image:#imageLiteral(resourceName: "catalog") ,tag:2)
                tabViewController3.tabBarItem = UITabBarItem(title: "Contact",image:#imageLiteral(resourceName: "contact") ,tag:3)
                
                tabBarController.viewControllers = [tabViewController1,tabViewController2,tabViewController3]
                
                window?.rootViewController = UINavigationController(rootViewController: tabBarController)
                window?.makeKeyAndVisible()*/
                
                
            }
        }
        
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      
        if AuthModel.sharedInstance.role == kRetailer {
            if UserDefaults.standard.object(forKey: kStartDate) != nil {
                let date = Date()
                UserDefaults.standard.set(date, forKey: kEndDate)
                
                callWebServiceForViewTime()
            }
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if AuthModel.sharedInstance.role == kRetailer {
            if UserDefaults.standard.object(forKey: kStartDate) != nil {
                let date = Date()
                UserDefaults.standard.set(date, forKey: kStartDate)
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //MARK: - Custom Methods
    func setLoginData() {
        
        let defaults = UserDefaults.standard
        let auth = AuthModel.sharedInstance
        
        auth.name = defaults.value(forKey: "name") as! String
        auth.role = defaults.value(forKey: "role") as! String
        auth.company = defaults.value(forKey: "company") as! String
        auth.email = defaults.value(forKey: "email") as! String
        auth.address = defaults.value(forKey: "address") as! String
        auth.device_token = defaults.value(forKey: "device_token") as! String
        auth.device_id = defaults.value(forKey: "device_id") as! String
        auth.created_at = defaults.value(forKey: "created_at") as! String
        auth.image = defaults.value(forKey: "image") as! String
        auth.updated_at = defaults.value(forKey: "updated_at") as! String
        auth.id = defaults.value(forKey: "id") as! String
        auth.email_verified_at = defaults.value(forKey: "email_verified_at") as! String
    }

    func callWebServiceForViewTime() {
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second]
        
        let date1 = UserDefaults.standard.object(forKey: kStartDate) as! Date
        let date2 = UserDefaults.standard.object(forKey: kEndDate) as! Date
        let catID = UserDefaults.standard.object(forKey: kCatalogIDForTimer) as! String

        let timeInSeconds = formatter.string(from: date1, to: date2)! as! String
        
        let param = ["catalogId": catID,
                     "timeInSecond": timeInSeconds,
                     "userId": AuthModel.sharedInstance.id]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.addCatalogViewTime, param: param as [String : Any], withHeader: true ) { (response, errorMsg) in
            
            if response == nil {
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    
                }
                else {
                }
            }
        }
    }

}

